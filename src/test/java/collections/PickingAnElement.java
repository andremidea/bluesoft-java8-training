package collections;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.fail;

public class PickingAnElement {

    final List<String> friends =
            Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");


    @Test
    public void devePegarOPrimeiroNomeQueComeceComALetraEPrintar(){
        pickName(friends, "N");
        pickName(friends, "Z");
    }

    //Escreva outro método pickName para usar as o findFirst e o Optional.
    @Test
    public void devePegarOPrimeiroNomeQueComeceComALetraEPrintarUsandoFuncional(){
        fail("Não foi implementado");
    }

    //Escreva outro método pickName para usar as o findFirst e o Optional, nesse caso use o ifPresent.
    @Test
    public void devePegarOPrimeiroNomeQueComeceComALetraEPrintarUsandoFuncionalUsandoIfPresent(){
        fail("Não foi implementado");
    }

    public static void pickName(final List<String> names, final String startingLetter) {
        String foundName = null;
        for(String name : names) {
            if(name.startsWith(startingLetter)) {
                foundName = name;
                break;
            }
        }
        System.out.print(String.format("A name starting with %s: ", startingLetter));

        if(foundName != null) {
            System.out.println(foundName);
        } else {
            System.out.println("No name found");
        }
    }

}


