package collections;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.fail;

public class IteratingList {


    final List<String> friends =
            Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");

    @Test
    public void devePrintarUsandoImpertiva(){
        for(String name : friends) {
            System.out.println(name);
        }
    }


    @Test
    public void devePrintarUsandoFuncionalUsandoClasseAnonima(){
        //new Consumer<String>().....
        fail("Não foi implementado");
    }

    @Test
    public void devePrintarUsandoFuncionalComHighOrderFunction(){
        //friend -> ...
        fail("Não foi implementado");
    }


    @Test
    public void devePrintarUsandoMethodReference(){
        //System.out::....
        fail("Não foi implementado");
    }


}
