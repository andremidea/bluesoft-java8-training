package collections;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.fail;

public class FindingElements {

    final List<String> friends =
            Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");

    @Test
    public void deveEncontrarTodosOsNomesComecadosComNDeFormaImperativa(){
        final List<String> startsWithN = new ArrayList<String>();
        for(String name : friends) {
            if(name.startsWith("N")) {
                startsWithN.add(name);
            }
        }

        System.out.println(String.format("Found %d names", startsWithN.size()));

    }

    //Usando filter e collect
    @Test
    public void deveEncontrarTodosOsNomesComecadosComNDeFormaFuncional(){

        fail("nao implementado");
    }




}
