package collections;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.Assert.fail;

public class ReusingLambdaExpression {

    final List<String> friends =
            Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");

    @Test
    public void deveEncontrarTodosOsNomesQueComecamComNeB(){
        final Predicate<String> comecamComN = name -> name.startsWith("N");
        final Predicate<String> comecamComB = name -> name.startsWith("B");

        final long countFriendsStartN = friends.stream().filter(comecamComN).count();
        final long countFriendsStartB = friends.stream().filter(comecamComB).count();


        System.out.println("Total de nomes começados com N: " + countFriendsStartN);
        System.out.println("Total de nomes começados com B: " + countFriendsStartB);

    }

    //Ao invés de criar 2 expressões, utilize a mesma apenas passando parametro. Crie uma função Static
    @Test
    public void deveEncontrarTodosOsNomesQueComecamComNeBReutilizandoAExpressao(){
        fail("Não implementado");
    }

    //Ao invés de criar 2 expressões, utilize a mesma apenas passando parametro. Crie uma variável que tenha expressão nela, use uma expressão que retorne um Predicate.
    @Test
    public void deveEncontrarTodosOsNomesQueComecamComNeBReutilizandoAExpressaoMasSemCriarMetodoStatico(){
        fail("Não implementado");
    }

    //Ao invés de criar 2 expressões, utilize a mesma apenas passando parametro. Crie uma variável que tenha expressão nela, não instancie nada só use expressões.
    @Test
    public void deveEncontrarTodosOsNomesQueComecamComNeBReutilizandoAExpressaoMasSemCriarMetodoStaticoESemClasseAnonima() {
        fail("Não implementado");
    }


}
