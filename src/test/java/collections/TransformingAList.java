package collections;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.fail;

public class TransformingAList {

    final List<String> friends =
            Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");

    @Test
    public void deveColocarONomeEmLetraMaisculaEPrintarImperativa(){
        final List<String> uppercaseNames = new ArrayList<String>();

        for(String name : friends) {
            uppercaseNames.add(name.toUpperCase());
        }

        for (String uppercaseName : uppercaseNames) {
            System.out.println(uppercaseName);
        }

    }

    //Só usar o do friends, e usar um for depois.
    @Test
    public void deveColocarONomeEmLetraMaisculaEPrintarDeFormaFuncionalSoComForEach(){
        final List<String> uppercaseNames = new ArrayList<String>();

        //popular uppercase

        for (String uppercaseName : uppercaseNames) {
            System.out.println(uppercaseName);
        }


        fail("nao implementado");
    }

    //Mapear a lista e printar depois
    @Test
    public void deveColocarONomeEmLetraMaisculaEPrintarDeFormaFuncionalEmUmaLinha(){
        fail("nao implementado");
    }


    //Mapear a lista e printar depois usando apenas Method Reference
    @Test
    public void deveColocarONomeEmLetraMaisculaEPrintarDeFormaFuncionalComMethodReference(){
        fail("nao implementado");
    }



}
