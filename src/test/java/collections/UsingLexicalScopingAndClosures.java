package collections;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.fail;

public class UsingLexicalScopingAndClosures {

    final List<String> friends =
            Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");
    final List<String> editors =
            Arrays.asList("Brian", "Jackie", "John", "Mike");
    final List<String> comrades =
            Arrays.asList("Kate", "Ken", "Nick", "Paula", "Zach");

    @Test
    public void deveEncontrarTodosOsNomesQueComecaoComNEscrevendoSempreAMesmaExpressao(){
        final long countFriendsStartN = friends.stream()
                .filter(name -> name.startsWith("N")).count();
        final long countEditorsStartN = editors.stream()
                .filter(name -> name.startsWith("N")).count();
        final long countComradesStartN = comrades.stream()
                .filter(name -> name.startsWith("N")).count();

        System.out.println("Total de nomes começados com N: "+(countComradesStartN+countEditorsStartN+countFriendsStartN));

    }

    //Extraia a expressão que filtra e reuse em todos os filtros.
    @Test
    public void deveEncontrarTodosOsNomesComecadosComNReusandoAExpressao(){
        fail("nao implementado");
    }




}
